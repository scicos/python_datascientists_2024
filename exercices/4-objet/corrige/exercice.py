# une proposition de solution
# (pas dans l'examen)
class MailingList:
    def __init__(self, mails, ignored):
        # copy() pour assurer que la liste dans l'objet
        # ne pointe pas sur un objet externe
        self.__ignored = ignored.copy()
        self.__mails = list(self.__merge(mails).difference(set(ignored)))

    def __merge(self,  mailLists ):
        result = set()
        for mailList in mailLists:
            result = result.union( set(mailList) )
        return result

    def add(self, mails):
        self.__mails = self.__merge([self.__mails, mails]).difference(set(ignored))

    def get(self):
        return self.__mails.copy()

    def getContaining(self, substring):
        res = []
        for val in self.__mails:
            if substring in val:
                res.append(val)
        # Ou
        # res = [ val for val in self.__mails if substring in val ]
        return res



mails1 = [ 'alice1@example.com', 'bob2@example.com',
  'carol3@example.com' ]
mails2 = [ 'alice1@example.com', 'mallory11@example.com',
  'oscar12@example.com' ]
mails3 = [ 'alice1@example.com', 'bob2@example.com',
  'trent14@example.com' ]
ignored = [ 'alice1@example.com', 'victor15@example.com',
  'yvonne22@example.com', 'oscar12@example.com' ]

ml = MailingList([mails1, mails2, mails3], ignored)
print(ml.get())
#=> ['bob2@example.com', 'carol3@example.com',
# 'mallory11@example.com', 'trent14@example.com']
ml.add(['greg23@example.ch', 'john42@example.ch'])
print(ml.get())
print(ml.getContaining('example.ch'))
#=> ['greg23@example.ch', 'john42@example.ch']
