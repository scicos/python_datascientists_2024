## Exercice

Ecrire une classe `MailingList` permettant de stocker et d'accéder à des adresses email.

Le constructeur doit prendre en paramètre une liste de listes de mails, pouvant contenir des doublons, et une liste de mails `ignored` devant être ignorés.

Vous devez stocker la liste de mails ignorés, et les emails non ignorés en évitant les doublons.

Vous ajouterez à cette classe les méthodes :

 - `add( mails )` qui permet d'ajouter une liste de mails, en ignorant les doublons et les mails ignorés
 - `get()` qui retourne une liste de tous les emails conservés
 - `getContaining( substring )` qui retourne tous les mails contenant `substring`

**Exemple d'utilisation**


```python
mails1 = [ 'alice1@example.com', 'bob2@example.com', 
  'carol3@example.com' ]
mails2 = [ 'alice1@example.com', 'mallory11@example.com', 
  'oscar12@example.com' ]
mails3 = [ 'alice1@example.com', 'bob2@example.com', 
  'trent14@example.com' ]
ignored = [ 'alice1@example.com', 'victor15@example.com', 
  'yvonne22@example.com', 'oscar12@example.com' ]

ml = MailingList([mails1, mails2, mails3], ignored)
print(ml.get()) 
#=> ['bob2@example.com', 'carol3@example.com', 
# 'mallory11@example.com', 'trent14@example.com']
ml.add(['greg23@example.ch', 'john42@example.ch'])
print(ml.getContaining('example.ch')) 
#=> ['greg23@example.ch', 'john42@example.ch']
```
