## Exercice

Soit le code suivant, utilisant les packages `numpy` et `pandas`

```python
import numpy as np
import pandas as pd

array = np.array([[11, 22, 33], [44, 55, 66]])
dataframe = pd.DataFrame(array, columns=['Column_1', 
  'Column_2', 'Column_3'])
print(dataframe)
```

 - Créez un dossier nommé `numpandas` dans lequel vous mettez le code précédent dans le fichier `programme.py`
 - Créez un environnement pour ce projet avec `pipenv`
 - Installez les packages nécessaires avec `pipenv` et faites fonctionner le code
 - Copiez les fichiers `Pipfile`, `Pipfile.lock` et `programme.py` dans un autre dossier (**pas** un sous-dossier), et créez un nouvel environnement pour faire fonctionner le code.