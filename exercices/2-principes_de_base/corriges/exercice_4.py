def isPrime(N):
    if N < 2:
        return False
    for i in range(2, N):
        if N % i == 0:
            return False
    return True

a = int(input("Entrez une borne inférieure: "))
b = int(input("Entrez une borne supérieure: "))

for i in range(a, b):
    if isPrime(i):
        print(i)