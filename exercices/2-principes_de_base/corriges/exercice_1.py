# 3 solutions pour la fonction leapYear
def leapYear(year):
    res = False
    if year % 4 == 0:
        res = True
    if year % 100 == 0:
        res = False
    if year % 400 == 0:
        res = True
    return res

def leapYear2(year):
    if year % 4 == 0 and year % 100 != 0:
        return True
    if year % 400 == 0:
        return True
    return False

def leapYear3(year):
    return (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0) 

# fonction qui retourne le nombre de jours dans un mois
def daysInMonth(year, month):
    if month == 4 or month == 6 or month == 9 or month == 11:
        return 30
    if month == 2:
        if leapYear(year):
            return 29
        return 28
    return 31

# fonction qui vérifie la validité d'une date
def dateValid(year, month, day):
    if year < 0 or year > 9999:
        return False
    if month < 1 or month > 12:
        return False
    num_days = daysInMonth(year, month)
    if day < 1 or day > num_days:
        return False
    return True 


print(dateValid(2001, 2, 29))
