import random

bound = int(input("Entrez la borne supérieure: "))
N = random.randint(0, bound)

guess = int(input("Devinez le nombre: "))
cpt = 1
while guess != N:
    if guess < N:
        print("Trop petit")
    else:
        print("Trop grand")
    cpt += 1
    guess = int(input("Devinez le nombre: "))

print("Bravo, vous avez trouvé le nombre en", cpt, "coups")
