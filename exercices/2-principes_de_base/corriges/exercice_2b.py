def leapYear(year):
    res = False
    if year % 4 == 0:
        res = True
    if year % 100 == 0:
        res = False
    if year % 400 == 0:
        res = True
    return res

def leapYear2(year):
    if year % 4 == 0 and year % 100 != 0:
        return True
    if year % 400 == 0:
        return True
    return False

def leapYear3(year):
    return (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0) 

def daysInMonth(year, month):
    if month == 4 or month == 6 or month == 9 or month == 11:
        return 30
    if month == 2:
        if leapYear(year):
            return 29
        return 28
    return 31

def dateValid(year, month, day):
    if year < 0 or year > 9999:
        return False
    if month < 1 or month > 12:
        return False
    num_days = daysInMonth(year, month)
    if day < 1 or day > num_days:
        return False
    return True

# parcourir toutes les dates potentielles
# des années 2023 et 2024 et afficher
# les dates valides
for year in range(2023, 2025):
    for month in range(1, 13):
        for day in range(1,32):
            if dateValid(year, month, day):
                print(year,"/",month,"/",day)