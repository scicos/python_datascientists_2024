from modules.dates import *

def askDate():
    y = int(input("Entrez une année "))
    m = int(input("Entrez un mois "))
    d = int(input("Entrez un jour "))
    return y, m, d

year, month, day = askDate()
while dateValid(year, month, day) == False:
    print("Veuillez entrer une date valide")
    year, month, day = askDate()

print("La date est valide")