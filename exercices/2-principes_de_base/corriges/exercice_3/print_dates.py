from modules.dates import *

for year in range(2023, 2024):
    for month in range(1, 13):
        for day in range(1,32):
            if dateValid(year, month, day):
                print(year,"/",month,"/",day)