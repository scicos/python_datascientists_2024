## Exercice 1a

 - Définir une fonction `leapYear` qui retourne `True` si une date est bissextile et `False` sinon.
 - Elle prend un argument: `year` (sur 4 chiffres)

Exemple d'utilisation:

```python
if leapYear( 1977 ):  
  print( "Année bissextile" )
else:
  print( "Année non bissextile" )
```

Quelques indications :

 - Les années bissextiles sont les années divisbles par 4 mais pas par 100, et les années divisibles par 400

## Exercice 1b

 - Définir une fonction `daysInMonth` qui retourne le nombre de jours dans un mois donné.
 - Elle prend deux arguments: `year` (sur 4 chiffres) et `month`

Exemple d'utilisation:

```python
print( "Nombre de jours en mars 1977: ", daysInMonth( 1977, 3 ) )
```

Quelques indications :

 - Avril, juin, septembre et novembre ont 30 jours
 - Février à 29 jours les années bissextiles, 28 sinon
 - Les autres mois ont 31 jours


## Exercice 1c

 - Définir une fonction `dateValid` qui retourne `True` si une date est possible et `False` sinon.
 - Elle prend trois arguments: `year` (sur 4 chiffres), `month` et `day`

Exemple d'utilisation:

```python
if dateValid( 1977, 3, 21 ):  
  print( "Date valide !" )
else:
  print( "Date invalide !" )
```

Quelques indications :

 - L'année doit être comprise entre 0 et 9999
 - Utilisez les fonctions `leapYear` et `daysInMonth` définies précédemment.