## Exercice 4 - nombres premiers

Ecrire une fonction `isPrime` qui prend en paramètre un nombre entier positif et qui retourne `True` si le nombre passé en paramètre est premier, et `False` sinon. Elle retourne `False` également si le nombre passé en paramètre est plus petit ou égal à 0.

Pour rappel, un nombre premier est un nombre divisible uniquement pas 1 et par lui-même.

Utilisez ensuite cette fonction pour écrire un programme complet qui affiche tous les nombres premiers entre une borne inférieure (incluse) et supérieure (non incluse). Le programme demandera à l'utilisateur d'entrer les bornes et affichera les nombres premiers entre ces deux bornes.
