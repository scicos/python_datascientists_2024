## Exercice 2a

Ecrire un programme Python complet qui demande une date à l'utilisateur. Le programme demandera une nouvelle date tant que l'utilisateur n'entre pas une date valide (boucle *while*).

Vous pouvez (devez) réutiliser les fonctions écrites dans les exercices précédents.


## Exercice 2b

Ecrire un programme qui écrit toutes les dates valides des années 2023 et 2024. Pour cela, faites des boucles imbriquées qui parcourent les dates possibles (boucles *for*) et testez si chaque date est valide avant de l'afficher.
daysInMonth` définies précédemment.
