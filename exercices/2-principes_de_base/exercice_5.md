## Exercice 5 - le nombre secret

Le but de cet exercice est de programmer le jeu du "nombre secret". Le but est de faire
chercher à l'utilisateur un nombre déterminé aléatoirement par
l'ordinateur. Le nombre de coups mis pour trouver la solution sera compté et
affiché en fin de la partie. La borne maximum pour le choix du nombre caché
sera demandée à l'utilisateur.

Pour effectuer ce travail, vous avez besoin de pouvoir générer des nombres aléatoires. En python, vous pouvez utiliser la librairie random pour cela

```python
>>> import random
```

et vous pouvez générer un nombre entier entre deux bornes (incluses) avec

```python
random.randint(2, 12)
```

la documentation se trouve ici [https://docs.python.org/3/library/random.html](https://docs.python.org/3/library/random.html).

Votre programme doit :

 - Demander une borne haute à l'utilisateur $N$
 - Tirer un nombre aléatoire entre 0 et $N$
 - Demander à l'utilisateur de deviner le nombre 
 - Indiquer si le nombre à trouver est plus grand ou plus petit que le choix de l'utilisateur
 - Le programme s'arrête lorsque l'utilisateur trouve le nombre
 - A ce moment, le programme affiche combien d'essais ont étés nécessaires pour trouver le nombre