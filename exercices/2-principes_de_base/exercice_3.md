## Exercice 3

Reprenez l'exercice 2 et modularisez le. Faites en sorte d'avoir deux programmes Python disctincts :

  - `ask_date.py` qui demande à l'utilisateur une date et valide l'entrée, puis affiche la date valide.
  - `print_dates.py` qui affiche toutes les dates valides de 2023 et 2024.

Vous ne devez pas avoir de code dupliqué. Placez toutes les fonctions nécessaires (`dateValid` et ses dépendances) dans un fichier séparé que vous importez.
