## Exercice 3

**Longueur des mots**

 - Ecrire une fonction `wordLen` qui prend une liste de chaînes de
caractère et retourne une nouvelle liste contenant la taille de
chaque chaîne.

```python
ys = wordLen( ["un", "deux", "trois", "quatre", "cinq"] )
print( ys ) #Affiche [2, 4, 5, 6, 4 ]
```
