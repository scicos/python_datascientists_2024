## Exercice 1

 - Définir la fonction findMax qui retourne la position du plus
grand élément d’une liste contenant des nombres.
 - La liste ne doit pas être modifiée
 - Que faire si la liste est vide ?

```python
i = findMax( [12, 3, 16, -15, 22, 8, -9] )
print(i) # Affiche 4
```
