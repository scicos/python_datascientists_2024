## Exercice 5

 - Définir la fonction `histogram(word)` qui va retourner un
dictionnaire dont les clés sont les éléments d'une collection
itérable et les valeurs, le nombre d'occurence de chaque
élément.
 - Si la collection est vide, le dictionnaire aussi.

```python
histogram( "ABRACADABRA" )
  #=> { "C": 1, "A":5, "B":2, "R": 2, "D", 1 }

histogram( ["foo", "bar", "foo", "baz", "foo"] )
  #=> { "foo": 3, "bar":1, "baz":1 }
```
