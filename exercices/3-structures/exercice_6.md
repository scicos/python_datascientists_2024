## Exercice 6 - les palindromes

Ecrire une fonction `isPalindrome` qui prend en paramètre une chaine de caractère et retourne `True` si la chaine est un parlindrome, `False` sinon.

Pour rappel, un palindrome est un mot qui se lit de la même façon de gauche à droite et de droite à gauche. Par exemple, "kayak" est un parlindrome. Par contre, "bonjour" n'en est pas un. La casse doit être prise en compte par votre fonction. Par exemple, "KayaK" doit être considéré comme un palindrome, mais pas "Kayak".

Utilisez votre fonction pour écrire un programme qui demande à l'utilisateur des mots et lui indique si il s'agit de palindromes. Le programme continuera a demander des mots à l'utilisateur tant que celui-ci n'entre pas le mot "quitter".

Exemple d'utilisation :

```
$ python palindrome.py
Entrez un mot : kayak
kayak est un palindrome
Entrez un mot : Kayak
Kayak n'est pas un palindrome
Entrez un mot : RADAR
RADAR est un palindrome
Entrez un mot : quitter
Fin du programme
$
```
