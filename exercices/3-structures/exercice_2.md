## Exercice 2

**Positifs uniquement**

 - Ecrire une fonction `positiveOnly` qui prends une liste de
nombres et retourne une nouvelle liste ne contenant que les
nombres positifs ou nuls:
 - La liste initiale ne doit pas être modifiée.

```python
xs = positiveOnly( [-1, 3, -3, 1, 3, -2 ] )
print( xs ) #Affiche [3, 1, 3]
```
