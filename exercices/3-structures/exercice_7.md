## Exercice 7a - crible d'Ératosthène

Le crible d'Ératosthène est une méthode permettant de générer les nombres premiers de 2 à $N$ à l'aide d'une table de booléens. $N$ étant un nombre entier naturel.

Pour générer les nombres premiers inférieurs à $N$, on alloue une liste de $N$ booléens initialisé à `True`. Ainsi, l'état initial  de la liste sera le suivant pour $N=12$ si on considère que 't' veut dire `True` et 'f' veut dire `False` :

| indice | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 |
|---     |---|---|---|---|---|---|---|---|---|---|----|----|
| valeur | t | t | t | t | t | t | t | t | t | t | t  | t  |

Après exécution de l'algorithme, l'état du tableau sera le suivant :

| indice | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 |
|---     |---|---|---|---|---|---|---|---|---|---|----|----|
| valeur | t | t | t | t | f | t | f | t | f | f | f  | t  |

Le cas du 0 et du 1 est un peu particuiler, car ils ne sont pas considérés comme premiers. On les conserve néanmoins dans cette représentation par raison de simplicité.

Pour arriver à ce résultat, l'algorithme parcours le tableau à partir de l'indice 2 avec une première boucle. Dès que l'on tombre sur une case `True`, l'algorithme parcours tous les multiples de ce nombre pour les passer à `False`. Ainsi, lors de la première itération, l'algorithme considère la case numéro 2 qui est à `vrai`, et invalide tous les multiples, à savoir 4, 6, 8 et 10.

Une représentation de ce processus est disponible sur wikipedia

[https://fr.wikipedia.org/wiki/Crible_d'Ératosthène](https://fr.wikipedia.org/wiki/Crible_d'Ératosthène).

Créez une fonction `eratosthene`qui prend la borne haute N (non incluse) et retourne une liste de N éléments (de 0 à N-1) sur laquelle le crible d'Ératosthène a été appliqué. Par exemple, `erathosthene(10)` retournera `[True, True, True, True, False, True, False, True, False, False]`.

# Exercice 7b

Modifiez la fonction `eratosthene` pour qu'elle retourne une liste de nombre premiers entre 2 et `N` (non inclus) plutôt qu'une liste de booléens. Par exemple, `erathosthene(10)` retournera maintenant [2, 3, 5, 7]. Si un nombre plus petit ou égal à 2 est entré, une liste vide est retournée.