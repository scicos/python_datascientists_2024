Pas exa

## Exercice 4

 - Soit 3 **listes** d'adresses mails provenant de trois carnets
d'adresses différents (`mails1`, `mails2`, `mails3`) et pouvant
contenir des doublons
 - Soit 1 **liste** d'adresses mail à ignorer (`ignored`)
 - Fusionner les 3 listes d'adresses en écartant celles qui doivent l'être.
 - Le résultat sera une **liste** sans doublon.
