# deux solutions pour générer un histogramme
# avec des dictionnaires
def histogram( elements ):
    res = {}
    for c in elements:
        if c in res:
            res[c] += 1
        else:
            res[c] = 1
    return res

def histogram2( elements ):
    res = {}
    for c in elements:
        res[c] = res.get(c, 0)+1
    return res

print(histogram( "ABRACADABRA" ))
print(histogram(["foo", "bar", "foo", "baz", "foo"]))

print(histogram2( "ABRACADABRA" ))
print(histogram2(["foo", "bar", "foo", "baz", "foo"]))