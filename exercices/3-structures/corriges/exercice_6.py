def isPalindrome(word):
    for i in range(len(word)):
        if word[i] != word[len(word)-1-i]:
            return False
    return True

def isPalindrome2(word):
    return word == word[::-1]

w = input("Entrez un mot : ")
while w != "quitter":
    if isPalindrome(w):
        print("Le mot est un palindrome")
    else:
        print("Le mot n'est pas un palindrome")
    w = input("Entrez un mot : ")
print("Fin du programme")