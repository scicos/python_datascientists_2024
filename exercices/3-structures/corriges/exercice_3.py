# extraire la longueur des mots d'une liste
# de mots
def wordLen(words):
    res = []
    for word in words:
        res.append(len(word))
    return res

# solution avec list comprehension
# (pas dans l'examen')
def wordLen2(words):
    return [len(word) for word in words]

ys = wordLen( ["un", "deux", "trois", "quatre", "cinq"] )
print( ys ) 

ys = wordLen2( ["un", "deux", "trois", "quatre", "cinq"] )
print( ys ) 