# extraire les nombres >= 0 d'une liste
def positiveOnly(xs):
    res = []
    for x in xs:
        if x >= 0:
            res.append(x)
    return res

# solution avec list comprehension
# (pas dans l'examen)
def positiveOnly2(xs):
    return [x for x in xs if x >= 0]


xs = positiveOnly( [-1, 3, -3, 1, 3, -2, 0 ] )
print( xs ) 
xs = positiveOnly( [] )
print( xs ) 

xs = positiveOnly2( [-1, 3, -3, 1, 3, -2, 0 ] )
print( xs ) 
xs = positiveOnly2( [] )
print( xs ) 