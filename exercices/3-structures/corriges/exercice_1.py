# 3 solutions pour la fonction findMax
def findMax(xs):
    if len(xs) == 0:
        return -1
    res = 0
    current_pos = 0
    max_elem = xs[0]
    for x in xs:
        if x > max_elem:
            res = current_pos
            max_elem = x
        current_pos += 1
    return res

def findMax2(xs):
    if len(xs) == 0:
        return -1
    res = 0
    max_elem = xs[0]
    for i in range(1, len(xs)):
        if xs[i] > max_elem:
            res = i
            max_elem = xs[i]
    return res

def findMax3(xs):
    if len(xs) == 0:
        return -1
    return xs.index(max(xs))

i = findMax( [12, 3, 16, -15, 48, 22, 8, -9,] )
print(i)
i = findMax( [] )
print(i)

i = findMax2( [12, 3, 16, -15, 48, 22, 8, -9,] )
print(i)
i = findMax2( [] )
print(i)

i = findMax3( [12, 3, 16, -15, 48, 22, 8, -9,] )
print(i)
i = findMax3( [] )
print(i)
