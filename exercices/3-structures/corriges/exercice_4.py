mails1 = [
    'alice1@example.com', 'bob2@example.com', 'carol3@example.com',
    'dave4@example.com', 'eve5@example.com', 'frank6@example.com',
    'grace7@example.com', 'heidi8@example.com', 'ivan9@example.com',
    'judy10@example.com', 'eve5@example.com', 'ivan9@example.com'
]

mails2 = [
    'alice1@example.com', 'mallory11@example.com', 'oscar12@example.com',
    'peggy13@example.com', 'trent14@example.com', 'victor15@example.com',
    'walter16@example.com', 'xavier17@example.com', 'yolanda18@example.com',
    'zara19@example.com', 'xavier17@example.com'
]

mails3 = [
    'alice1@example.com', 'bob2@example.com', 'trent14@example.com',
    'ursula20@example.com', 'victor15@example.com', 'wendy21@example.com',
    'xavier17@example.com', 'yvonne22@example.com', 'zack23@example.com',
    'zoe24@example.com', 'ursula20@example.com', 'trent14@example.com',
    'yvonne22@example.com'
]

ignored = ['alice1@example.com', 'victor15@example.com', 'yvonne22@example.com', 'trent14@example.com']

# fusionner des listes de mails en utilisant des sets
# (sets pas dans l'examen)
def merge(emails, ignored):
    res = set()
    for mail_list in emails:
        res = res.union(set(mail_list))
    res = res.difference(ignored)
    return list(res)


mailing_list = merge([mails1, mails2, mails3], ignored)
print(mailing_list)

if len(mailing_list) > len(set(mailing_list)):
    print("Il y a des doublons")

for mail in ignored:
    if mail in mailing_list:
        print("Il a des gens a enlever")