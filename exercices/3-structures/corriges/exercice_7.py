def eratosthene7a(n):
    # creation d'une liste de n booleens
    # initialises a vrai
    premiers = [True] * n
    for i in range(2, n):
        if premiers[i]:
            j = 2*i
            while j < n:
                premiers[j] = False
                j += i
    return premiers

def eratosthene7b(n):
    if n <= 2:
        return []
    premiers = eratosthene7a(n)
    res = []
    for i in range(2, n):
        if premiers[i]:
            res.append(i)
    return res

def eratosthene7b2(n):
    if n <= 2:
        return []
    premiers = eratosthene7a(n)
    return [i for i in range(2, n) if premiers[i]]

print(eratosthene7a(10))
print(eratosthene7b(10))
print(eratosthene7b2(10))
print(eratosthene7b(2))