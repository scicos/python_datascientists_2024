## Exercice 3

Installez Pandas dans votre environnement.

Des résultats d'un test pour une maladie `M` sont effectués sur des patients.
On sait qu'un individu est probablement porteur de la maladie `M` si la
concentration du marqueur dépasse un seuil `S` donné.

Les tests sont délégués à des laboratoires quelconque. 
Les laboratoires retournent les résultats sous forme de fichiers CSV, contenant:
- numéro AVS
- prénom
- nom
- année de naissance (simplification de la date de naissance)
- genre (ici uniquement `F` ou `M`)
- concentration d'un marqueur (mg/L)

## Lecture des données dans un Dataframe

Vous devez importer ces fichier avec Pandas en utilisant le numéro AVS comme 
index, voici un petit exemple qui illustre la forme des données (trois entrées):
```
                   last_name gender  mg/L  year_of_birth
106.7875.7793.42  Villasenor      F  0.98           1988
173.7816.6745.35        Koch      F  1.60           1975
333.8325.9627.88      Rowden      M  2.34           2014
```

Les données sont stockées dans plusieurs fichiers CSV, et tous doivent être lus
dans un unique Dataframe.

Pour ceci utliser les fonctions Pandas suivantes:

- pour lire un fichier CSV en Dataframe Pandas: `pd.read_csv(filename, index_col = "avs")` 
où `filename` est le nom du fichier à lire. La documentation de la fonction:

<https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html>

- pour concaténer des Dataframes Pandas (càd. les assembler les uns à la suite 
des autres en un unique Dataframe): `full_df = pd.concat([df1, df2, df3])` où
`[df1, df2, df3]` est une liste de Dataframes à concaténer (conserve l'ordre,
mais sans importance ici). La documentation de la fonction:

<>https://pandas.pydata.org/docs/reference/api/pandas.concat.html

- pour créer un Dataframe Pandas vide: `pd.DataFrame()` (si nécessaire)

Par exemple:
```Python
empty_df = pd.DataFrame()
new_df = pd.read_csv(filename, index_col = "avs")
full_df = pd.concat([empty_df, new_df], ignore_index=True)
```

Ceci pouvant être répété autant de fois qu'il y a de Dataframe à concaténer
(càd. "à assembler").
Typiquement pour chaque fichier lu.

### Aide: lire des fichiers d'un répertoire donnée avec Python

Si nécessaire, pour vous aidez à lire les fichiers d'un répertoire donné, vous
pouvez utiliser ou vous inspirer de code suivant:

```Python
import os

def read_all_csv(directory: str):
  # liste tous les fichier d'un répertoire
  # et mets les noms de fichiers dans une liste
  files = os.listdir(directory)
  # pour chaque nom de fichier dans "directory"
  for f in files:
    # si le fichier se termine par .csv
    if f.endswith('.csv'):
      # on construit le nom complet du fichier CSV trouvé
      csv_f = directory + "/" + f
      # toute action nécessaire ici avec le fichier, ici on print
      print(f"CSV file found: {csv_f}")
```

Si vous avez besoin d'aide, n'hésitez pas à demander à un des cadres du 
séminaire.

## Traitement des données

Pour des raisons de statistique (p.ex. étude épidémiologique), on souhaite avoir:

- les concentrations moyennes des femmes malades
- les concentrations moyennes des hommes malades
- les concentrations moyennes des enfants de moins de 16 ans malades
- le pourcentage de patients malades (ayant fait le test)

Le tout dans un nouveau Dataframe, de la forme:

```
  gender      mg/L        age  group
0      F  2.024074  22.962963    all
1      M  2.098400  16.440000    all
2    F+M  2.170000   8.750000  young
```

L'ordre des colonnes importe peux, mais il contient:
- le genre: F (female), M (male), F+M (both)
- le groupe d'age: all (adultes et enfants), young (enfants)
- l'age moyen du groupe
- la concentration moyenne du groupe

Ensuite enregistrez dans un nouveau fichier utilisable avec un tableur.
Pour ce dernier point, jetez un oeil à la documentation de
- `pd.DataFrame.to_csv`
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_csv.html>

Vous pouvez même sauver votre Dataframe au format tableur, de type "Excel":
- `pandas.DataFrame.to_excel`
- <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_excel.html>
