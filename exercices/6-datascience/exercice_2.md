## Exercice 2

Installez Matplotlib dans votre environnement.

Reprenez la matrice de vente de l'exercice 1 et faites un graphique de nombre
d'objets vendus par mois.
Sur l'axe X les mois doivent être représentés par leurs numéros: 1 pour janvier,
2 février, ..., 12 décembre.

Voici un exemple de rendu du graphique désiré:

![Exemple du résultat attendu.](fig/ex_1.png)

Produisez un graphique où l'on voit également le cummul du nombre d'objets
vendus avec comme titre "Ventes 2023" et une légende.
Jetez un oeil à la documentation de Matplotlib pour trouver comment faire:

- <https://matplotlib.org/stable/api/index.html>

Voici un exemple de rendu du graphique désiré:

![Exemple du résultat attendu.](fig/ex_2.png)

Si on manque de temps pour les exercices, jetez un oeil aux fonctions:
- `legend`: <https://matplotlib.org/stable/api/legend_api.html>
- `title` <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.title.html>

Sauvez également votre figure en PDF, pour ceci en place de faire un `plt.show()`,
utilisez:

- `savefig`: <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.savefig.html>

Comme par exemple:

```Python
plt.savefig(filename, format="pdf", bbox_inches="tight")
```

où le mode `tight` supprime l'espace blanc inutile autour de votre figure.
