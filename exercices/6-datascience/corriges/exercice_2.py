import numpy as np
import matplotlib.pyplot as plt

ventes = ... # voir énoncé exercice 1

# Graphique de nombre d'objets vendus par mois. 
objs_per_month = np.sum(ventes[:,1:], axis=0)
months = np.arange(1, 13)
plt.plot(months, objs_per_month)
plt.xlabel('Mois')
plt.ylabel('Nombre d\'objets vendus')
plt.show()

# Graphique du cummul d'objets vendus par mois.
cum_objs_per_month = np.cumsum(objs_per_month)
plt.plot(months, cum_objs_per_month)
plt.xlabel('Mois')
plt.ylabel('Cummul d\'objets vendus')
plt.show()

# Les deux courbes sur le même graphique.
plt.plot(months, objs_per_month, label='Nombre d\'objets vendus')
plt.plot(months, cum_objs_per_month, label='Cummul d\'objets vendus')
plt.xlabel('Mois')
plt.legend()
plt.title('Ventes 2024')
plt.savefig("vente_2024.pdf", format="pdf", bbox_inches="tight")
