import numpy as np

ventes = ... # voir énoncé

prices = ventes[:,0]
quant_by_obj = np.sum(ventes[:,1:], axis=1)
money = prices*quant_by_obj

# Combien rapport le produit le plus d'argent au commerce sur l'année ?
q1 = np.max(money)

# Quel est le gain total des ventes du commerce sur l'année ?
q2 = np.sum(money)

# Combien d'objets ont été vendu pour avril, mai et juin ?
q3 = np.sum(ventes[:,5:8], axis=0)
