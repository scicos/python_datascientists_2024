import pandas as pd
import matplotlib.pyplot as plt
import os

directory = "..."

files = os.listdir(directory)
df = pd.DataFrame()
for f in files:
  if f.endswith('.csv'):
    ff = directory + "/" + f
    df = pd.concat([df, pd.read_csv(ff, index_col = "avs")])


df['sick'] = df['mg/L'] > 1.5
# bonus, avec le apply
# df["age"] = df["year_of_birth"].apply(lambda x: 2024 - int(x))
df['age'] =  2024 - df["year_of_birth"].to_numpy()
df['young'] = df['age'] < 16

sick_ppl = df[df['sick']]
agg_cols = ['mg/L', 'age']
gnder_age = sick_ppl.groupby(['gender'], as_index=False)[agg_cols].mean()
young_old = sick_ppl.groupby(['young'], as_index=False)[agg_cols].mean()
young_old = young_old[young_old['young']]
young_old['gender'] = "F+M"
young_old = young_old.drop(columns=['young'])

# le ignore_index=True permet de ne pas garder les index des groupes
# sinon on risque d'avoir des doublons (testez et vous verrez !)
data = pd.concat([gnder_age, young_old], ignore_index=True)
data['group'] = ["all", "all", "young"]

data.to_csv('processed_data.csv', index=False)
