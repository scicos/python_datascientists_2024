## Exercice 1

Installez NumPy dans votre environnement.

**Vous ne pouvez pas utiliser de boucles pour cet exercice !**

On suppose qu'une matrice représente les ventes de `n` produits d'un
commerce.

Cette matrice est de dimension `n x 13` car chaque rangée représente un
des produits vendu où:

- la première colonne est le prix du produit en CHF,
- les douze colonnes suivantes indiquent le nombre d'unités vendues par mois au cours
  d'une année (de janvier à décembre).

Voici un code suivant pour générer une matrice de ce type, ici pour 27 produits:

```Python
import numpy as np
# nombre de produits différent
n = 10
# on choisit le prix qui varie de 2.00 à 99.90 CHF
prix = np.round(np.random.uniform(2,100, (n,1)),1)
# la quantité de produit vendue par mois varie de 0 à 30 unités
quant = np.random.randint(0, 30, (n,12))
# concaténation des colonnes des deux matrices
ventes = np.c_[prix, quant]
```

voici à quoi ressemble la matrice avec 10 produits:
```
>>> ventes
array([[30.2,  8. ,  6. ,  9. , 24. , 16. , 19. ,  2. , 29. ,  2. , 29. ,  8. , 22. ],
       [47.1,  4. , 14. ,  7. , 14. , 16. ,  0. , 27. , 20. ,  5. , 24. ,  9. , 14. ],
       [11.1, 27. , 25. , 28. , 19. , 20. , 14. , 19. , 24. , 17. , 23. , 24. ,  4. ],
       [28.1,  2. ,  8. ,  3. , 29. , 23. , 16. , 29. , 12. ,  4. , 18. ,  2. , 17. ],
       [24.9, 23. , 14. , 15. , 24. ,  2. , 24. ,  2. , 11. ,  3. , 24. ,  2. , 18. ],
       [34.9, 25. , 19. , 22. ,  7. ,  7. ,  1. ,  6. , 10. , 23. , 27. ,  3. ,  3. ],
       [85.3,  0. ,  1. ,  1. , 27. ,  5. ,  8. , 21. , 23. , 13. ,  2. ,  7. , 16. ],
       [74.3, 15. , 17. , 16. , 13. ,  0. ,  8. ,  1. , 28. ,  3. , 27. ,  1. , 25. ],
       [50.1, 21. , 26. , 28. , 23. ,  0. ,  4. ,  7. , 15. , 26. ,  5. , 25. , 20. ],
       [80.1, 28. , 16. , 10. ,  0. ,  9. , 18. , 25. , 17. , 26. , 24. ,  7. , 20. ]])
```

Le type de la matrice et ses "dimensions":
```
>>> ventes.dtype
dtype('float64')
>>> ventes.shape
(10, 13)
``` 
correspondent bien à ce qui est attendu.

## Manipulation des matrice NumPy

**Sans utiliser de boucle**, répondez aux questions suivantes:

1. Quel produit rapport le plus d'argent au commerce sur l'année ?
   Il n'est pas nécessaire de localiser l'indice du produit, mais uniquement donner
   la valeur en CHF.

Exemple de réponse avec la matrice générée ci-dessus:
```
>>> q1
np.float64(16019.999999999998)
```

2. Quel est le gain total des ventes du commerce sur l'année ?

Exemple de réponse avec la matrice générée ci-dessus:
```
>>> q2
np.float64(77229.8)
```

3. Combien d'objets ont été vendu par mois, en avril, mai et juin ? Donner
   uniquement la quantité.

```
>>> q3
array([ 98., 112., 139.])
```
